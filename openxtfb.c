// 
// OpenXT Framebuffer: PV framebuffer driver for the OpenXT display handler
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
// Author: Kyle J. Temkin  <temkink@ainfosec.com>
// Author: Rian Quinn      <quinnr@ainfosec.com>
//

//Enable debugging.
//#define OPENXTFB_DEBUG


/******************************************************************************/
/* Includes                                                                   */
/******************************************************************************/

//Linux kernel includes.
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/fb.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/uaccess.h>

//Helper for generic PV display communications
#include "pv_display_helper.h"

//Module-specific includes
#include "openxtfb.h"


/******************************************************************************/
/* Module Parameters                                                          */
/******************************************************************************/

//Display domain: the domain ID in which the Display Handler will be running.
//This eventually should be replaced by another method of querying the display
//server, such as UUID-- to support an external display domain.
static int display_domain = 0;
module_param(display_domain, int, S_IRUGO);

//Display port: the port that IVC will use to connect to control channel of the
//Display Handler.
static int control_port = 1000;
module_param(control_port, int, S_IRUGO);

//Maximum framebuffers: the maximum number of framebuffers the module should
//advertise support for. Currently viewed as a hint, rather than a restriction.
static int advertised_framebuffers = 1;
module_param(advertised_framebuffers, int, S_IRUGO);

//Specify whether hints should be ignored.
//If set, this ignores all display size hints and uses the default resolution.
static int ignore_hints = 0;
module_param(ignore_hints, int, S_IRUGO | S_IWUSR);

//Specify whether display keys should be ignored.
//If set, this means that an OpenXT framebuffer will accept any commands sent
//to it, no matter the key. This is only useful in the case where we have a
//single framebuffer that we may want to move from monitor to monitor.
//If set to -1, the framebuffer will decide autmatically: if we have a single
//advertised framebuffer, if will ignore keys; otherwise it will enforce them.
static int ignore_keys = -1;
module_param(ignore_keys, int, S_IRUGO | S_IWUSR);

//Specify the allocated virtual width, in pixels. Allocating a larger
//virtual width will allow the framebuffer to grow after its initial connect,
//but will also use up kernel memory.
//
//Note that the virtual width can be overridden by the display handler
//on its initial connect-- so a machine with a larger monitor can increase
//this size. (If you're having difficulty with this behavior-- e.g. your
//system can't provide enough space for the framebuffer, you can set
//the ignore_hints flag to ignore the larger framebuffer size; or
//try upping your vmalloc allcation!)
static int virtual_width = 1920;
module_param(virtual_width, int, S_IRUGO | S_IWUSR);

//Specify the allocated virtual height.
//See the warning for the virtual width.
static int virtual_height = 1080;
module_param(virtual_height, int, S_IRUGO | S_IWUSR);

//Determines whether the initial connection attempt
//should be re-attempted. If this value is nonzero, the module will
//periodically attempt to connect to the display handler if no initial
//connection is made.
static int retry_initial_connect = 1;
module_param(retry_initial_connect, int, S_IRUGO | S_IWUSR);

//Specify the delay (in jiffies) between reconnect attempts.
//After a disconnect, this specifies the polling interval at which the module
//will look for the display handler.
static int reconnect_delay = HZ * 10;
module_param(reconnect_delay, int, S_IRUGO | S_IWUSR);

//Specifies the delay after which dirty rectangles are computed and delivered.
//This defaults to generating events roughly 25 times per second.
static unsigned long rectangle_delay = HZ / 25;
module_param(rectangle_delay, ulong, S_IRUGO | S_IWUSR);


/**
 * @return The domid for the domain in which the Display Handler will be
 *  running.
 *
 * This method abstracts away the method by which we get the display domain's
 * ID, so we can eventually replace the display_domain arguemnt with a more
 * static identifier (e.g. UUID, if we wanted to switch to a dedicated display
 * domain).
 */
static inline int __get_display_domid(void) {
  return display_domain;
}


/******************************************************************************/
/* Data Structures                                                            */
/******************************************************************************/

/**
 * Stores a "size hint", which relates a Display Handler key to a desired
 * resolution. Used to determine the resolution upon creating a new display.
 */
struct openxtfb_display_size_hint {

    //Linked list entry, which allows us to string together size hints
    //into a list.
    struct list_head node;

    //The Display Handler key-- which uniquely identifies the given display.
    uint32_t key;

    //The desired width and height of the given display.
    uint32_t width;
    uint32_t height;
};


/**
 * Stores information regarding an instance of the OpenXT framebuffer
 * module. Abstracts away state, in case we ever want to suppor more than
 * one of these modules.
 */
struct openxtfb_instance {

    //A lock that guards the OpenXT FB instance.
    struct mutex lock;

    //The PV display provider object that handles DH interactions for this
    //kernel module.
    struct pv_display_provider * provider;

    //Keeps track of all PV display objects created for the OpenXT framebuffer
    //platform device. This is effectively a list of all OpenXT framebuffer
    //devices-- active or otherwise.
    struct list_head displays;

    //A linked list of display size "hints", which are used as suggestions for
    //the resolution of the created displays.
    struct list_head display_hints;

    //Stores information about any active deferred reconnect attempt.
    struct delayed_work reconnect_work;
};


/**
 * As part of our platform device state, we store a list of all PV displays
 * (framebuffers) associated with the given display. This simple structure
 * wraps our display objects, and allows us to add them to our linked list.
 *
 * This seems much cleaner than the alternatives-- for example, overloading
 * the private data at the end of a framebuffer object.
 *
 * You should hold the instance lock before manipulating this list, or any
 * of its elements.
 */
struct display_list_element {
    struct pv_display * display;
    struct list_head node;
    bool connected;
};

/******************************************************************************/
/* Module Singletons                                                          */
/******************************************************************************/

//Store the (singleton) state of our platform device.
static struct platform_device * openxtfb_device;


/******************************************************************************/
/* Forward Declarations                                                       */
/******************************************************************************/

//See below.
static void openxtfb_handle_display_error(struct pv_display * display, void* userData);
static int __openxtfb_finalize_connection(struct pv_display_provider * provider);
static void openxtfb_defer_connect(int delay);
static void openxtfb_send_hotplug_event(void);

/******************************************************************************/
/* Deferred IO                                                                */
/******************************************************************************/


/**
 * Invalidates (sends a dirty rectangle for) the minimum rectangle that contains
 * a pair of bytes. Bytes are represented as their offset into the framebuffer.
 *
 * @param info The framebuffer for which the given 
 * @param min_offset The lesser of the two bytes.
 * @param max_offset The greater of the two bytes.
 */
static void openxtfb_invalidate_region_between_offsets(struct fb_info *info,
    uintptr_t min_offset, uintptr_t max_offset) {

    struct pv_display * display = info->par;

    //Assuming we have a valid dirty region, we'll convert it into a set
    //of rectangles, and send an dirty event region over IVC.
    if (min_offset < max_offset) {

        //Find the Y locations of our minimum and maximum pages.
        //As our framebuffers are row-major, we can locate the row these
        //are in by dividing by the row length.
        uint32_t y1 = min_offset / info->fix.line_length;
        uint32_t y2 = max_offset / info->fix.line_length;

        //... and do the same for the X locations. (Note that in the case
        //that we're being called by the deferred I/O routine, these are
        //always guaranteed to be page aligned)
        uint32_t x1 = (min_offset % info->fix.line_length);
        uint32_t x2 = (max_offset % info->fix.line_length);

        //Compute the actual bounding rectangles for the points computed.
        uint32_t x = min(x1, x2);
        uint32_t y = min(y1, y2);
        uint32_t width  = (x2 > x1) ? (x2 - x1 + 1) : (x1 - x2 + 1);
        uint32_t height = (y2 > y1) ? (y2 - y1 + 1) : (y1 - y2 + 1);

        // Finally, report the dirty region.
        display->invalidate_region(display, x, y, width, height);
    }
}


/**
 * Handle Deferred IO Events: detects changes in the framebuffer, and converts
 * them to dirty rectangle events.
 *
 * The Deferred IO API gives us a list of pages within the framebuffer that have
 * been touched recently-- typically so we can commit changes from a backing
 * framebuffer in system memory to a video device across a slow bus. While we
 * don't have to perform any copying (we've effectively shared our "backing" \
 * store), we take advantage of this API to get a list of dirty pages to update.
 *
 * @param info The framebuffer information for the framebuffer being monitored.
 * @param pagelist A linked list describing each of the page objects that have
 *    been touched.
 */
static void openxtfb_deferred_io(struct fb_info *info,
    struct list_head * pagelist)
{
    struct page * page;
    struct pv_display * display;
    uintptr_t max_offset = 0;
    uintptr_t min_offset = (uintptr_t)-1;

    openxtfb_checkp(info);
    openxtfb_checkp(pagelist);
    openxtfb_checkp(info->par);

    //Ensure that the rectangle delay is never allowed to be zero, which
    //doesn't make sense as a scheduler tick delay. Bump it up to the default
    //value used by defio.
    if(rectangle_delay == 0)
      rectangle_delay = HZ;

    //Copy in the latest value for the rectangle delay, which can be adjusted
    //at runtime. This allows user performance tuning.
    info->fbdefio->delay = rectangle_delay;

    //Get a reference to the PV display associated with this framebuffer.
    display = info->par;

    //Iterate over the list of all touched pages, and try to determine
    //which regions are dirty from the pages that are touched.
    list_for_each_entry(page, pagelist, lru) {

        //First, compute the offsets of the first and last pixels in the page.
        uintptr_t first_pixel_in_page = page->index << PAGE_SHIFT;
        uintptr_t last_pixel_in_page  = first_pixel_in_page + PAGE_SIZE - 1;

        //And try to find the two extreme offsets, which will give us
        //the outermost points of our dirty region.
        min_offset = min(min_offset, first_pixel_in_page);
        max_offset = max(max_offset, last_pixel_in_page);
    }

    //Assuming we have a valid dirty region, we'll convert it into a set
    //of rectangles, and send an dirty event region over IVC>
    if (min_offset < max_offset) {
        //Find the Y locations of our minimum and maximum pages.
        //As our framebuffers are row-major, we can locate the row these
        //are in by dividing by the row length.
        uint32_t y1 = min_offset / info->fix.line_length;
        uint32_t y2 = max_offset / info->fix.line_length;

        //... and do the same for the X locations, ensuring that the values we
        //are providing are aligned to the pages provided by the deferred IO
        //routine.
        uint32_t x1 = (min_offset % info->fix.line_length) & ~PAGE_MASK;
        uint32_t x2 = PAGE_ALIGN(max_offset % info->fix.line_length) - 1;

        // Report the dirty region.
        display->invalidate_region(display, x1, y1, x2 - x1 + 1, y2 - y1 + 1);
    }
}

/**
 * Define the handler for our deferred I/O operations.
 */
static struct fb_deferred_io openxtfb_defio = {
    .deferred_io    = openxtfb_deferred_io
};

/******************************************************************************/
/* FB_OPS Functions                                                           */
/******************************************************************************/

/**
 * Validate a new frambuffer variable info ("var") structure. Allows the
 * framebuffer to validate (and modify) a collection of settings passed in from
 * the userspace.
 *
 * @param var The variable data structure to be validated / modified.
 * @param info The information for the framebuffer to be modified.
 * @return 0 on success, or an error code on failure.
 */
static int openxtfb_check_var(struct fb_var_screeninfo *var,
    struct fb_info * info)
{
    uint32_t x_resolution; 
    uint32_t y_resolution;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(var, -EINVAL);
    openxtfb_checkp(info, -EINVAL);

    //We only allow modification of our basic resolution-- that is,
    //the width and height /used/ of our framebuffer object. To enforce this,
    //this method needs to update the passed-in var to revert any undesired 
    //changes.

    //First, we'll figure out the most acceptable X and Y resolution available.
    //TODO: We may want to look through a list of standard modes, rather than
    //cobbling together odd resolutions.
    x_resolution = min(var->xres, info->var.xres_virtual);
    y_resolution = min(var->yres, info->var.yres_virtual);

    //If we were passed a resolution with a zero width/height,
    //flat out reject it.
    if(!x_resolution || !y_resolution) {
        memcpy(var, &info->var, sizeof(*var));
        openxtfb_error("Tried to set a resolution with zero width/height!"
            " Rejecting the change.\n");
        return -EINVAL;
    }

    //Next, we'll keep the new X/Y resolution, but override all other fields.
    memcpy(var, &info->var, sizeof(*var));
    var->xres = x_resolution;
    var->yres = y_resolution;

    //Success!
    return 0;
}


/**
 * Updates the framebuffer status to deal with a change in the framebuffer's
 * variable info. Ideally, each of these variable information bits should be
 * displayable-- as we were able to validate and modify the values in the
 * function above.
 *
 * @param info The framebuffer information for the framebuffer whose parameters
 *    have just changed.
 * @return 0 on success, or an error code on failure.
 */
static int openxtfb_set_par(struct fb_info *info)
{
    struct pv_display * display;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(info, -EINVAL);

    //Get a refernece to the PV display driving this framebuffer.
    display = info->par;
    openxtfb_checkp(display, -EINVAL);

    //Notify the Display Handler of our new resolution/stride.
    display->change_resolution(display, info->var.xres,
        info->var.yres, display->stride);

    return 0;
}

/**
 * Scales a Linux colormap color to fit within a single byte.
 *
 * This particular implementation takes advantage of the fact
 * that all nibbles of a color-mapped chunk are the same; so
 * the provided values (e.g. 0xAAAA) can be converted to
 * equivalent colors just by truncating.
 *
 * @param color The color to be converted.
 * @return The color scaled so it can be used as a color
 *    "mask" byte in ARGB8888 coloring.
 */
static unsigned __scale_linux_color_to_byte(unsigned color)
{
    return color >> 8;
}


/**
 * Sets the framebuffer's color registers. Using for emulating limited-color
 * terminals on true-color (e.g. 24-bit color) framebuffers.
 *
 * @param regno The register number to set.
 * @param red The red component of the desired color.
 * @param green The green component of the desireed color.
 * @param blue The blue component of the desired color.
 * @param trams The alpha component of the desired color.
 * @param info The frambuffer info for which the register should be set.
 */
static int openxtfb_setcolreg(unsigned regno, unsigned red, unsigned green,
               unsigned blue, unsigned transp, struct fb_info *info)
{
    uint32_t * target_register;
    uint32_t color;

    //Linux, by default, produces 16-bit values for each of the colors
    //in its color map. We'll scale these down to byte sizes, so they
    //fit into the single bytes of our ARGB8888.
    red    = __scale_linux_color_to_byte(red);
    green  = __scale_linux_color_to_byte(green);
    blue   = __scale_linux_color_to_byte(blue);
    transp = __scale_linux_color_to_byte(transp);

    //Piece together the color from the scaled color components...
    color =
        (red << info->var.red.offset) |
        (green << info->var.green.offset) |
        (blue << info->var.blue.offset) |
        (transp << info->var.transp.offset);

    openxtfb_checkp(info, -EINVAL);

    //Ensure that we have a register number that's within our emulated
    //color palette: it must be within [0, openxtfb_palette_color_count).
    if (regno >= openxtfb_palette_color_count)
        return -EINVAL;

    //Get a pointer to the register in which the color should be stored...
    target_register = &((uint32_t *)info->pseudo_palette)[regno];

    //... and store the target color.
    *target_register = color;

    //Indicate success.
    return 0;
}

/**
 * Handles writes to the framebuffer using the write system call.
 *
 * These writes aren't common, and mostly occur from older utilities
 * which replace the whole framebuffer's contents. The recommended way
 * to work with a framebuffer is to mmap it into your process space.
 *
 * @param info The framebuffer information for the relevant framebuffer.
 * @param buf The userland buffer storing the data to be written.
 * @param count The total amount of bytes to be written.
 * @param ppos An in/out argument that stores the next position to be
 *    written. After a successful write, this points one byte past
 *    the last written byte.
 *
 * @return The total amount of bytes written on success, 0 if a valid
 *    reason caused us to not write, or a negative error code.
 *
 */
static ssize_t openxtfb_write(struct fb_info *info, const char __user *data,
    size_t count, loff_t *ppos)
{
    loff_t position = *ppos;
    int bytes_written, bytes_failed;
    char *dst;

    openxtfb_checkp(info, -ENODEV);
    openxtfb_checkp(info->screen_base, -ENODEV);

    //This logic is very similar to the fb_write() function from fbmem,
    //which is almost exactly what we need-- but which doesn't generate
    //a dirty rectangle, and which is optimized for IO memory.

    if(info->state != FBINFO_STATE_RUNNING)
        return -EPERM;

    //If we're trying to write more than will fit in the file, fail out.
    if(position > info->screen_size)
        return -EFBIG;

    //If we're trying to write past the end of the file, truncate.
    if(count + position > info->screen_size) {
        count = info->screen_size - position;

        //If we couldn't write anything, fail out!
        if(!count)
          return -ENOSPC;
    }

    //Determine the location in the framebuffer to which we'll be writing...
    dst = (char *)info->screen_base + position;

    //... and copy over the user's data.
    bytes_failed = copy_from_user(dst, data, count);

    //Compute the total number of bytes written, and increment
    //our position by that amount.
    bytes_written = count - bytes_failed;
    *ppos += bytes_written;

    //Finally, send off a dirty rectangle encapsulating the area
    //that has been overwritten.
    openxtfb_invalidate_region_between_offsets(info, position, *ppos - 1);

    //Return our status: either we were able to write data,
    //or we failed during copy_from_user.
    return bytes_written ? bytes_written : -EFAULT;
}


/**
 * Simple helper function that fills a relevant rectangle with a solid color.
 * Overridden so we can report the generated dirty rectangle.
 *
 * @param info The framebuffer information for the framebuffer to render on.
 * @param region A structure describing the region to be filled.
 */
static void openxtfb_fillrect(struct fb_info *info,
    const struct fb_fillrect *region)
{
    struct pv_display * display;

    openxtfb_checkp(info);
    openxtfb_checkp(region);
    openxtfb_checkp(info->par);

    //Get a reference to the PV display object providing our display.
    display = info->par;

    //Use the kernel-provided fillrect function, which fills a rectangle with a
    //given color. Note our use of the sys_ version, which is optimized for
    //buffers in system memory.
    sys_fillrect(info, region);

    //Finally, report the dirty region.
    display->invalidate_region(display, region->dx, region->dy,
        region->width, region->height);
}


/**
 * Simple helper funciton that copies a region of image data from one place in
 * the framebuffer to another. Overridden so we can report the generated dirty
 * rectangle.
 *
 * @param info The framebuffer information for the framebuffer to render on.
 * @param area A structure describing the area to be copied.
 */
static void openxtfb_copyarea(struct fb_info *info, const struct fb_copyarea *area)
{
    struct pv_display * display;

    openxtfb_checkp(info);
    openxtfb_checkp(area);
    openxtfb_checkp(info->par);

    //Get a reference to the PV display object providing our display.
    display = info->par;

    //Use the kernel-provided copyarea function, which copies one area of the framebuffer
    //to another. Note our use of the sys_ version, which is optimized for buffers in system memory.
    sys_copyarea(info, area);

    /* Report the dirty region */
    display->invalidate_region(display, area->dx, area->dy, area->width, area->height);
}


/**
 * Simple helper funciton that selectively copies portions of an image
 * ("block transfers" an image) onto the framebuffer. Overriden so we can report
 * the generated dirty rectangle.
 *
 * @param info The framebuffer information for the framebuffer to render on.
 * @param image The image to be blit'd onto the framebuffer.
 */
static void openxtfb_imageblit(struct fb_info *info, const struct fb_image * image)
{
    struct pv_display * display;

    openxtfb_checkp(info);
    openxtfb_checkp(image);
    openxtfb_checkp(info->par);

    //Get a reference to the PV display object providing our display.
    display = info->par;

    //Use the kernel-provided imageblit function, which selectively (block)
    //transfers "blits" image sections onto our framebuffer. Note our use of the
    //sys_ version, which is optimized for buffers in system memory.
    sys_imageblit(info, image);

    /* Report the dirty region */
    display->invalidate_region(display, image->dx, image->dy,
        image->width, image->height);
}


/**
 * Map an OpenXT Framebuffer into the userspace.
 *
 * Note that this is _not_ used when the deferred I/O API is on, as it overrides
 * our framebuffer's mmap routine with a working MMAP. This method is provided
 * for situations where it's desirable to turn of deferred I/O (e.g. for debug).
 *
 * Note that we can't use the default fb_mmap, as the pages we have aren't
 * physically contiguous. Instead, we'll have to manually insert each of the
 * virtual memory pages, one at a time.
 *
 * @param info The information structure ("framebuffer object") for the
 *     framebuffer to be mapped into the userspace.
 * @param vma The userspace virtual memory area we want to map our framebuffer
 *     into.
 *
 * @return 0 on success, or an error code on failure.
 */
static int openxtfb_mmap(struct fb_info * info, struct vm_area_struct * vma)
{
    int rc;

    uintptr_t next_to_map;
    struct page * page_to_map;

    //Get an easy reference to the largest possible virtual page address,
    //for our reference.
    const uintptr_t virtual_address_max = (~0UL >> PAGE_SHIFT);

    //Get the starting address for the virtual memory region...
    uintptr_t next_virtual_address = vma->vm_start;

    //... determine the size of the region to be allocated...
    uintptr_t data_to_map = vma->vm_end - vma->vm_start;

    //... and compute the total offset /into/ the framebuffer that the
    //user has requested in bytes.
    uintptr_t offset = vma->vm_pgoff << PAGE_SHIFT;

    __PV_HELPER_TRACE__;

    //
    //Sanity check the requested user addres.
    //

    //If the userland has provided a page offset beyond the end of virtual
    //memory, don't listen to the user. (This really should be subsumed by our
    //length checking, but other kernel modules check for this explicitly,
    //so we will too.)
    if (vma->vm_pgoff > virtual_address_max)
      return -EINVAL;

    //If the userland has requested more memory than we've allocated for
    //our framebuffer, fail out.
    if (data_to_map > info->fix.smem_len)
      return -EINVAL;

    //If the user has requested a mapping that extends beyond the length of
    //our framebuffer, fail out.
    if (offset + data_to_map > info->fix.smem_len)
      return -EINVAL;


    //
    // Map in the address itself:
    //

    //Begin mapping from the requested offset into our framebuffer.
    next_to_map = (uintptr_t)info->fix.smem_start + offset;

    //And continue mapping until we've used up all data to map.
    while (data_to_map > 0) {

      //Get a reference to the next page to be mapped...
      page_to_map = vmalloc_to_page((void *)next_to_map);

      //... and map it into the userspace!
      rc = vm_insert_page(vma, next_virtual_address, page_to_map);

      //If we failed, return the relevant error code.
      if(rc)
          return rc;

      //Move our virtual and physical addresses forward by a single page:
      //this sets us up to map the next page.
      next_to_map          += PAGE_SIZE;
      next_virtual_address += PAGE_SIZE;

      //If we have more than a page of data left to map,
      //decrease the total data left by a full page; otherwise,
      //stop mapping.
      if (likely(data_to_map > PAGE_SIZE))
          data_to_map -= PAGE_SIZE;
      else
          data_to_map = 0;
    }

    //Indicate success.
    return 0;
}


/**
 * Attempts to locate any size hints that exist for a given display
 * given its Display Handler key.
 *
 * @param instance The platforms instance associated with the desired
 *    hint.
 * @param key The Display Handler key identifying the display to locate.
 * @return The located hint object, or NULL if none was found.
 */
static struct openxtfb_display_size_hint * __find_size_hint(
    struct openxtfb_instance * instance, uint32_t key)
{
    struct openxtfb_display_size_hint * hint_entry;
    struct openxtfb_display_size_hint * target = NULL;

    mutex_lock(&instance->lock);

    //Iterate over each of the /existing/ hint entries...
    list_for_each_entry(hint_entry, &instance->display_hints, node) {

        //... until you find a display with the given key.
        if(hint_entry->key == key) {
            target = hint_entry;
            break;
        }
    }

    mutex_unlock(&instance->lock);
    return target;
}


/**
 * Attempts to determine an appropriate size for a given display, using the
 * following algorithm:
 *  -If a display hint has been set in the past, it will be used.
 *  -If no display hint has been set, this method will return a
 *   default width and height.
 *
 *  @param instance The platform instance object that stores data for this
 *      platform device.
 *  @param key The Display Handler key identifying the display who size
 *      should be determined.
 *  @param width An out argument that will receive the relevant width.
 *  @param height An out argument that will receive the relevant height.
 *  @return 1 if we're using the default resolution-- that is, if no
 *      hint was located; 0 on a valid lookup, and a negative error code on
 *      an error.
 */
static int __get_recommended_size_for_display(struct openxtfb_instance * instance,
    uint32_t key, uint32_t * width, uint32_t * height)
{
    struct openxtfb_display_size_hint * size_hint;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(width, -EINVAL);
    openxtfb_checkp(height, -EINVAL);

    //Next, check to see if we have a valid size hint.
    //If we have one, return its size.
    size_hint = __find_size_hint(instance, key);
    if(size_hint) {
        *width  = size_hint->width;
        *height = size_hint->height;
        return 0;
    }

    //If we weren't able to find anything, return the defaults.
    *width  = openxtfb_default_width_pixels;
    *height = openxtfb_default_height_pixels;
    return 1;
}


/**
 * Queries the given framebuffer for information about it's host
 * (the Display Handler)'s capabilities.
 *
 * @param display The display for which information is being queried.
 * @param openxtfb_caps The capabilities structure to be populated.
 */
static int openxtfb_ioctl_get_caps(struct pv_display *display,
        void __user *arg)
{
    struct openxtfb_caps caps;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(display, -EINVAL);
    memset(&caps, 0, sizeof (caps));

    caps.supports_cursor = display->supports_cursor(display);
    //Set the "supports cursor" field to indicate whether the given display
    //supports a cursor on the host side.
    if (copy_to_user(arg, &caps, sizeof (caps)))
        return -EFAULT;

    //Indicate success.
    return 0;
}


/**
 * Loads a cursor image into the OpenXT hardware cursor image,
 * if possible.
 *
 * @param display The display for which the cursor image is to be populated.
 * @param image The image to be loaded.
 */
static int openxtfb_ioctl_load_cursor_image(struct pv_display *display,
        const void __user *arg)
{
    int rc;
    size_t image_size;
    void *target_image;
    struct openxtfb_cursor_image image_info;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(display, -EINVAL);

    if (copy_from_user(&image_info, arg, sizeof (image_info)))
        return -EFAULT;

    //Create an buffer for an in-kernel copy of the cursor image...
    image_size = pixels_to_bytes(image_info.width * image_info.height);
    target_image = kmalloc(image_size, GFP_KERNEL);
    if(!target_image) {
        openxtfb_error("Could not allocate space for a PV cursor load!"
            " Continuing to use the old cursor.\n");
        return -ENOMEM;
    }

    if (copy_from_user(target_image, image_info.image, image_size)) {
        openxtfb_error("Could not obtain a copy of the PV cursor!\n");
        return -EFAULT;
    }

    //Load the cursor image into the PV cursor buffer.
    rc = display->load_cursor_image(display, target_image,
        image_info.width, image_info.height);

    kfree(target_image);
    return rc;
}

/**
 * Sets the properties of an OpenXT-FB cursor. For now, used to
 * set the cursor's hot-spot.
 *
 * @param display The display whose cursor is to be updated.
 * @param properties The properties to be set.
 */ 
static int openxtfb_ioctl_set_cursor_properties(struct pv_display * display,
        const void __user * arg)
{
    struct openxtfb_cursor_properties properties;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(display, -EINVAL);

    if (copy_from_user(&properties, arg, sizeof (properties)))
        return -EFAULT;

    //Update the cursor's hotspot.
    display->set_cursor_hotspot(display, properties.hotspot_x,
        properties.hotspot_y);

    //Indicate success.
    return 0;
}

/**
 * Sets the visibility of an OpenXT-FB cursor. 
 *
 * @param display The display whose cursor is to be updated.
 * @param visible True iff the cursor should be visible.
 */ 
static int openxtfb_ioctl_set_cursor_visibility(struct pv_display * display,
        bool visible)
{
    __PV_HELPER_TRACE__;
    openxtfb_checkp(display, -EINVAL);

    //Update the cursor's visibility.
    display->set_cursor_visibility(display, visible);

    //Indicate success.
    return 0;
}


/**
 * Sets the visibility of an OpenXT-FB cursor. 
 *
 * @param display The display whose cursor is to be updated.
 * @param visible True iff the cursor should be visible.
 */ 
static int openxtfb_ioctl_move_cursor(struct pv_display * display,
        const void __user *arg)
{
    struct openxtfb_cursor_location location;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(display, -EINVAL);

    if (copy_from_user(&location, arg, sizeof (location)))
        return -EFAULT;

    //Update the cursor's location.
    display->move_cursor(display, location.x, location.y);

    //Indicate success.
    return 0;
}


/**
 * Provides display resolution recommendations to a userspace process.
 *
 * @param display The display whose size is being queried.
 * @param hint The size hint to be filled.
 */
static int openxtfb_ioctl_get_size_hint(struct pv_display * display,
        struct fb_info * info, void __user *arg)
{
    int rc;
    uint32_t width, height;
    struct openxtfb_instance * instance = platform_get_drvdata(openxtfb_device);
    struct openxtfb_size_hint hint;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(display, -EINVAL);
    memset(&hint, 0, sizeof (hint));

    //Check to see if we have any size recommendations for this display.
    rc = __get_recommended_size_for_display(instance, display->key,
        &width, &height);

    //If we couldn't check for any recommendations, fail out.
    if(rc < 0)
        return -ENOENT;

    //Otherwise, populate the response's hint...
    hint.width  = width;
    hint.height = height;

    //... some basic information about the framebuffer...
    hint.max_width  = info->var.xres_virtual;
    hint.max_height = info->var.yres_virtual;
    hint.stride     = info->fix.line_length;

    //... and the source of our hint.
    hint.from_display_handler = (rc == 0);

    if (copy_to_user(arg, &hint, sizeof (hint)))
        return -EFAULT;

    //Indicate success.
    return 0;
}


/**
 * Handle custom OpenXT-FB IOCTLs. Currently used only for hardware cursor
 * support.
 *
 * @param info The framebuffer for which the ioctl is being issued.
 * @param cmd The IOCTL number for the ioctl being issued.
 * @param arg The opaque argument to the ioctl; typically a pointer.
 *
 */
static int openxtfb_ioctl(struct fb_info *info, unsigned int cmd,
    unsigned long arg)
{
    struct pv_display * display;
    void __user *p = (void __user *)arg;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(info, -EINVAL);
    openxtfb_checkp(info->par, -EINVAL);

    //Get a reference to the PV display object providing our display.
    display = info->par;

    //And handle the relevant IOCTL.
    switch(cmd)
    {

    case OPENXTFB_IOCTL_GET_CAPS:
      return openxtfb_ioctl_get_caps(display, p);

    case OPENXTFB_IOCTL_LOAD_CURSOR_IMAGE:
      return openxtfb_ioctl_load_cursor_image(display, p);

    case OPENXTFB_IOCTL_SET_CURSOR_PROPERTIES:
      return openxtfb_ioctl_set_cursor_properties(display, p);

    case OPENXTFB_IOCTL_SET_CURSOR_VISIBLITY:
      return openxtfb_ioctl_set_cursor_visibility(display, p);

    case OPENXTFB_IOCTL_MOVE_CURSOR:
      return openxtfb_ioctl_move_cursor(display, p);

    case OPENXTFB_IOCTL_GET_SIZE_HINT:
      return openxtfb_ioctl_get_size_hint(display, info, p);

    default:
        return -EINVAL;
    }
}


/**
 * Framebuffer operations for OpenXT framebuffers.
 */
static struct fb_ops openxtfb_ops = {

    .owner           = THIS_MODULE,

    //Provided functions: these functions are
    //handled manually by OpenXT-FB code.
    .fb_write        = openxtfb_write,
    .fb_check_var    = openxtfb_check_var,
    .fb_set_par      = openxtfb_set_par,
    .fb_setcolreg    = openxtfb_setcolreg,
    .fb_fillrect     = openxtfb_fillrect,
    .fb_copyarea     = openxtfb_copyarea,
    .fb_imageblit    = openxtfb_imageblit,
    .fb_mmap         = openxtfb_mmap,

    //Support custom IOCTLs, which are used for
    //(e.g.) PV cursor support.
    .fb_ioctl        = openxtfb_ioctl,

    //Default functions: these functions use
    //default implementation, provided by the fbdev
    //system.
    .fb_open         = NULL,
    .fb_release      = NULL,
    .fb_read         = NULL,
    .fb_setcmap      = NULL,
    .fb_cursor       = NULL,
    .fb_destroy      = NULL,
    .fb_blank        = NULL,
    .fb_pan_display  = NULL,
     // .fb_rotate       = NULL,
    .fb_sync         = NULL,
    .fb_get_caps     = NULL,
    .fb_debug_enter  = NULL,
    .fb_compat_ioctl = NULL,
    .fb_debug_leave  = NULL
};

/******************************************************************************/
/* Platform Functions                                                         */
/******************************************************************************/

/**
 * Initializes the "variable" (user modifiable) information for a given
 * framebuffer. This information can theoreticaly be modified by the user by
 * issuing an IOCTL, e.g. as performed by the fbset utility.
 *
 * These represent the current state of the framebuffer, including the
 * current mode-- though we force many of the following pieces to stay the same
 * in the check_par function above.
 *
 * @param info The framebuffer information to be filled.
 * @param width The maximum width of the framebuffer.
 * @param height The maximum height of the framebuffer.
 */
static void __initialize_framebuffer_variable_info(struct fb_info * info,
    uint32_t width, uint32_t height, uint32_t stride, uint32_t yres_virtual)
{
    //Compute a virtual width based on our stride. The user's virtual width
    //is modified slightly to page align the buffer's scanlines. This slightly
    //optimizes the case in which we're using temporal copies.
    uint32_t bytes_per_pixel = FB_BPP / 8;
    uint32_t xres_virtual = stride / bytes_per_pixel;

    /*
     * Fill in the fb_var_screeninfo structure. Note that I try to fill in
     * as much as possible for others. This structure is located in the fb.h
     * file, and a lot of the macros that are needed are there. Also note that
     * since this is a virtual device, we use ARGB format, but the alpha is
     * really going to be ignored, so we could turn that off. Finally, I left
     * off all of the clock timing information as that would all be 0, and
     * it can be found in the structure's definition.
     */
    __PV_HELPER_TRACE__;
    info->var.xres           = width;                           /* Default width */
    info->var.yres           = height;                          /* Default height */
    info->var.xres_virtual   = xres_virtual;                    /* Width of the allocated buffer */
    info->var.yres_virtual   = yres_virtual;                    /* Height of the allocated buffer */
    info->var.xoffset        = 0;                               /* X offset into virtual */
    info->var.yoffset        = 0;                               /* Y offset into virtual */
    info->var.bits_per_pixel = FB_BPP;                          /* Bits per pixel */
    info->var.grayscale      = 0;                               /* Color */
    info->var.transp         = (struct fb_bitfield){24, 0, 0};  /* Alpha is bits 31-24 */
    info->var.red            = (struct fb_bitfield){16, 8, 0};  /* Red is bits 23-16 */
    info->var.green          = (struct fb_bitfield){8, 8, 0};   /* Green is bits 15-8 */
    info->var.blue           = (struct fb_bitfield){0, 8, 0};   /* Blue is bits 7-0 */
    info->var.nonstd         = 0;                               /* Standard pixel format */
    info->var.activate       = FB_ACTIVATE_NOW;                 /* Set values immediately */
    info->var.width          = -1;                              /* Width in mm */
    info->var.height         = -1;                              /* Height in mm */
    info->var.accel_flags    = 0;                               /* Obsolete */
    info->var.vmode          = FB_VMODE_NONINTERLACED;          /* Don't interlace lines. */
}

/**
 * Initializes the fixed (not user modifiable) information regarding the
 * framebuffer. This information is expected to remain constant throughout the
 * framebuffer's lifetime.
 *
 * @param info The framebuffer information structure to be populated.
 * @param framebuffer A pointer to the raw framebuffer store used by this object.
 * @param framebuffer_size The total size of the raw framebuffer store, in bytes.
 * @param stride The stride of this framebuffer-- the total byte length of a
 *   single row of framebuffer data. Constant for all modes.
 */
static void __initialize_framebuffer_fixed_info(struct fb_info * info,
    void * framebuffer, size_t framebuffer_size, uint32_t stride)
{
    /*
     * Fill in the fb_fix_screeninfo structure. I was not able to find any
     * documentation as to why it is called "fix", but it is.
     */
    __PV_HELPER_TRACE__;
    strcpy(info->fix.id, "openxtfb");                           /* Id */
    info->fix.smem_start     = (unsigned long)framebuffer;      /* The address of the framebuffer to be shared... */
    info->fix.smem_len       = (unsigned long)framebuffer_size; /* And the size (in bytes) of the framebuffer to be shared. */
    info->fix.type           = FB_TYPE_PACKED_PIXELS;           /* Packed (as in ARGB is packed info one 32bit word) */
    info->fix.type_aux       = 0;                               /* Not needed for packed */
    info->fix.visual         = FB_VISUAL_TRUECOLOR;             /* See skeletonfb.c for an explanation */
    info->fix.xpanstep       = 0;                               /* Panning not supported in this driver */
    info->fix.ypanstep       = 0;                               /* Panning not supported in this driver */
    info->fix.ywrapstep      = 0;                               /* Wrapping not supported in this driver */
    info->fix.line_length    = stride;                          /* The image's stride. */
    info->fix.mmio_start     = 0;                               /* Not needed for virtual driver */
    info->fix.mmio_len       = 0;                               /* Not needed for virtual driver */
    info->fix.accel          = FB_ACCEL_NONE;                   /* No hardware acceleration supported */

}

/**
 * Create the framebuffer information structure-- which is essentially the linux kernel's
 * internal representation of a given framebuffer.
 *
 * @param display The PV display object for which the framebuffer
 *   information should be created.
 * @param device The linux device which owns the provided framebuffer.
 * @param framebuffer A pointer to the raw framebuffer that will be described
 *    by the info structure.
 * @param framebuffer_size The total size of the raw framebuffer store,
 *    in bytes.
 * @param width The maximum width of the framebuffer.
 * @param height The maximum height of the framebuffer.
 * @param stride The stride of this framebuffer-- the total byte length of a
 *    single row of framebuffer data. Constant for all modes.
 *
 * @return A pointer to a new fb_info object, or NULL if one could not
 *    be created.
 */
static struct fb_info *  __create_openxtfb_info(struct pv_display * display, struct device * device,
    void * framebuffer, size_t framebuffer_size, uint32_t width, uint32_t height, uint32_t stride,
    uint32_t yres_virtual)
{
    int rc;

    //Allocate a new framebuffer information structure-- the linux kernel's
    //core representation of a framebuffer.
    struct fb_info * info = framebuffer_alloc(0, device);

    __PV_HELPER_TRACE__;

    //If we weren't able to allocate space for the framebuffer, fail out early.
    if(unlikely(!info))
      return NULL;

    //Initialize the core framebuffer information-- both the variable (capable
    //of changing during the framebuffer's lifetime) and fixed (should never
    //change during the life of the framebuffer).
    __initialize_framebuffer_variable_info(info, width, height, stride, yres_virtual);
    __initialize_framebuffer_fixed_info(info, framebuffer, framebuffer_size, stride);

    //Set up the internal "raw" framebuffer information.
    info->screen_size = framebuffer_size;
    info->screen_base = framebuffer;

    //Register our framebuffer's private data.
    info->par = display;

    //Register each of our framebuffer "methods"-- the functions that will
    //handle the basic framebuffer operations.
    info->fbops = &openxtfb_ops;

    //Set the flags for the framebuffer, which will determine how the
    //framebuffer is handled by the system.
    info->flags = FBINFO_DEFAULT |              //Use the framebuffer defaults, but:
                  FBINFO_VIRTFB |               // -Optimize for a framebuffer in system RAM.
                  FBINFO_HWACCEL_COPYAREA |     // -Note that we provide the (required) copyarea function...
                  FBINFO_HWACCEL_FILLRECT |     // -... the required fillrect function...
                  FBINFO_HWACCEL_IMAGEBLIT |    // -... and the imageblit function.
                  FBINFO_MISC_ALWAYS_SETPAR;    // - Request that the kernel always set_par on a console switch (for safety).

    //Create a psuedo-palette store, which stores the relationships between
    //limited-color consoles and ARGB color values.
    info->pseudo_palette =
        kzalloc(sizeof(uint32_t) * openxtfb_palette_color_count, GFP_KERNEL);

    //If we weren't able to create the psuedo palette, fail out.
    if(unlikely(!info->pseudo_palette)) {
        openxtfb_error("Could not allocate a color map for this function!");
        goto clean_up_fb;
    }

    //Allocate a color map for our new framebuffer.
    rc = fb_alloc_cmap(&info->cmap, openxtfb_palette_color_count, 0);

    //If we weren't able to allocate a color map, fail out.
    if(unlikely(rc)) {
        openxtfb_error("Could not allocate a color map for this function!");
        goto clean_up_palette;
    }

    //Set the "period" that should elapse between subsequent calculations of
    //dirty rectangles. This effectively sets how often rectangles are uploaded
    //to the display handler.
    openxtfb_defio.delay = rectangle_delay;

    //Enable the deferred IO api, which will allow us to track dirty pages
    //as the framebuffer is (asynchronously) modified. See the deferred IO
    //section above for more information.
    info->fbdefio = &openxtfb_defio;
    fb_deferred_io_init(info);

    //Return the newly-constructed framebuffer.
    return info;

clean_up_palette:
    kfree(info->pseudo_palette);
clean_up_fb:
    framebuffer_release(info);
    return NULL;
}


/**
 * Registers a new display object. This allows us to locate the
 * display on reconnect, and to find the display on cleanup.
 * 
 * This method should be called for all newly created displays.
 */
static int __register_display_object(struct pv_display * display) 
{
    struct openxtfb_instance *instance = platform_get_drvdata(openxtfb_device);

    //Create a new list element to store a record of our new display.
    struct display_list_element *entry = kzalloc(sizeof(*entry), GFP_KERNEL);

    __PV_HELPER_TRACE__;

    if(!entry) {
        openxtfb_error("Could not register a display object! We may leak memory. \n");
        return -ENOMEM;
    }

    //... and bind our new display to it.
    entry->display = display;
    entry->connected = true;

    //Finally, add it to the list of all known display objects.
    mutex_lock(&instance->lock);
    list_add(&entry->node, &instance->displays);
    mutex_unlock(&instance->lock);

    //Indicate success.
    return 0;
}


/**
 * Unregisters a display object, removing it from the list of known
 * displays. Should be called on display teardown.
 *
 * @param display The display to be removed.
 */
static void __update_display_connection_status(struct pv_display * display, bool connected)
{
    struct openxtfb_instance *instance = platform_get_drvdata(openxtfb_device);
    struct display_list_element *entry, *temp;
    mutex_lock(&instance->lock);

    //Iterate over each of the /existing/ display entries...
    list_for_each_entry_safe(entry, temp, &instance->displays, node) {

        //Once we've found the current display, update its status.
        if(entry->display == display) {
            entry->connected = connected;
            break;
        }
    }

    mutex_unlock(&instance->lock);
}


/**
 * Unregisters a display object, removing it from the list of known
 * displays. Should be called on display teardown.
 *
 * @param display The display to be removed.
 */
static void __unregister_display_object(struct pv_display * display)
{
    struct openxtfb_instance *instance = platform_get_drvdata(openxtfb_device);
    struct display_list_element *entry, *temp;
    mutex_lock(&instance->lock);

    //Iterate over each of the /existing/ display entries...
    list_for_each_entry_safe(entry, temp, &instance->displays, node) {

        //... and delete any instances of the current display.
        if(entry->display == display) {
            list_del(&entry->node);
        }
    }

    mutex_unlock(&instance->lock);
}


/**
 * Attempts to located an existing pv_display object based on its key.
 *
 * @param instance The platforms instance that should own the given
 *    pv_display object.
 * @param key The Display Handler key identifying the display to locate.
 * @return The located pv_display object, or NULL if none was found.
 */
static struct pv_display *  __find_add_display_target(struct openxtfb_instance * instance,
    uint32_t key)
{
    struct display_list_element * entry;
    struct pv_display * target = NULL;

    mutex_lock(&instance->lock);

    //Iterate over each of the /existing/ hint entries...
    list_for_each_entry(entry, &instance->displays, node) {

        bool key_matches = (entry->display->key == key);

        //... until you find an acceptable display.
        if(key_matches || ignore_keys) {
            target     = entry->display;
            break;
        }
    }

    mutex_unlock(&instance->lock);
    return target;
}



/**
 * Cleans up an OpenXT PV display, and its associated framebuffer--
 * freeing all memory associated with the given display.
 *
 * @param display The display to be cleaned up.
 * @return 0 if the display could be cleaned up, or an error code if the display
 *    still exists and is valid.
 */
int openxtfb_clean_up_display(struct pv_display * display)
{
    struct openxtfb_instance *instance;
    struct fb_info * info;
    int rc;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(display, -EINVAL);

    instance = platform_get_drvdata(openxtfb_device);

    //Get the framebuffer information structure associated with the given display.
    info = display->get_driver_data(display);
    openxtfb_checkp(info, -EINVAL);

    //Attempt to unregister the framebuffer-- this effectively stops use of the
    //framebuffer without actually tearing the framebuffer down.
    unregister_framebuffer(info);

    //Remove the framebuffer from our list of all displays.
    __unregister_display_object(display);

    //Now that we've unregistered the given framebuffer, we can savely tear
    //down its objects.
    fb_deferred_io_cleanup(info);

    //Next, deallocate all of the resources associated with the
    //given framebuffer.
    kfree(info->pseudo_palette);
    fb_dealloc_cmap(&info->cmap);
    framebuffer_release(info);

    //Finally, destroy the relevant display object.
    display->destroy(display);

    //Indicate success.
    return 0;
}



/**
 * Creates an OpenXT framebuffer, allocating and registering all relevant objects.
 * Results in a fully working framebuffer accessible under /dev/fbX.
 *
 * @param provider The PV display provider which should be used to create
 *    this framebuffer.
 * @param request The PV display request which describes the display to be
 *    created.
 * @param width The width of the desired framebuffer.
 * @param height The height of the desired framebuffer.
 */
static int openxtfb_create_framebuffer(struct pv_display_provider * provider,
    struct dh_add_display * request, uint32_t width, uint32_t height)
{
    int rc;

    struct fb_info *info = NULL;
    struct device *device = NULL;
    struct pv_display *display;
    uint32_t desired_width, desired_height, stride;

    //Ensure that our virtual width and height meet our sanity checks.
    //These are here mostly to prevent horrible integer overflows.
    if(virtual_width > openxtfb_max_width)
        virtual_width = openxtfb_max_width;
    if(virtual_height > openxtfb_max_height)
        virtual_height = openxtfb_max_height;

    //Determine the desired width. Normally, we'll allocate more space
    //than necessary, to enable the framebuffer to grow if the connector
    //is attached to a larger monitor.
    desired_width  = max((uint32_t)virtual_width,  width);
    desired_height = max((uint32_t)virtual_height, height);

    //Determine the stride that the given framebuffer should have--
    //aligning each scanline to a page boundary. This marginally improves
    //the speed of copying in and out of the framebuffer.
    stride = PAGE_ALIGN(pixels_to_bytes(desired_width));

    __PV_HELPER_TRACE__;

    //Request that the PV Display Provider create a new display object--
    //which we'll wrap with this framebuffer.
    rc = provider->create_display(provider, &display, request, desired_width,
        desired_height, stride, NULL);

    //If we weren't able to create the display object, we won't be able to bring
    //up a framebuffer. Fail out!
    if(unlikely(rc)) {
        openxtfb_error("Failed to create a PV display object!\n");
        return rc;
    }

    //Allocate our internal representation of the OpenXT framebuffer device.
    info = __create_openxtfb_info(display, device, display->framebuffer,
        display->framebuffer_size, width, height,
        display->stride, desired_height);

    //If we weren't able to set up our OpenXT info, bail out!
    if(unlikely(!info)) {
        openxtfb_error("Failed to allocate local space for an openxtfb object!\n");
        display->destroy(display);
        return -ENOMEM;
    }

    //Tie our framebuffer information to the given display. We'll later use
    //this for display cleanup.
    display->set_driver_data(display, info);

    //Register our display fatal error handler, which will handle any issues
    //with the display connection.
    display->register_fatal_error_handler(display, (void*) openxtfb_handle_display_error);

    //Finally, register the framebuffer with the linux kernel, making it
    //ready for use!
    rc = register_framebuffer(info);

    //If we weren't able to register the framebuffer, bail out.
    if (unlikely(rc < 0)) {
        openxtfb_error("Failed to register a framebuffer!\n");

        //Clean up the allocated objects, and return.
        fb_dealloc_cmap(&info->cmap);
        framebuffer_release(info);
        display->destroy(display);
        return rc;
    }

    //Once we've registered a framebuffer, add our display object to the
    //list of active displays.
    __register_display_object(display);

    //Finally, notify the Display Handler to expect our new resolution.
    rc = display->change_resolution(display, width, height, stride);

    //If we couldn't set the resolution, fail out!
    if(unlikely(rc)) {
        openxtfb_error("Failed to set the framebuffer's resolution!\n");
        return -EAGAIN;
    }

    //Indicate success.
    return 0;
}


/**
 * Creates an OpenXT framebuffer, allocating and registering all relevant objects.
 * Results in a fully working framebuffer accessible under /dev/fbX.
 *
 * @param provider The PV display provider which should be used to create
 *    this framebuffer.
 * @param display The PV display object whose backend connections should be
 *    recreated.
 * @param request The PV display request which describes the display to be
 *    created.
 */
static int openxtfb_reconnect_display_to_backend(struct pv_display * display,
    struct dh_add_display * request)
{
    int rc;

    __PV_HELPER_TRACE__;

    //Request that the display handler create a new connection to this display.
    rc = display->reconnect(display, request, __get_display_domid());

    //If we weren't able to create the display object, we won't be able to bring
    //up a framebuffer. Fail out!
    if(unlikely(rc)) {
        openxtfb_error("Failed to reconnect to display %u!\n",
            (unsigned int)request->key);
        return rc;
    }

    //... and update the display key, which may have changed; particularly if we
    //are ignoring keys.
    display->key = request->key;

    //Mark the display as connected.
    __update_display_connection_status(display, true);

    //Notify the Display Handler that we're ready
    //for our new display to be activated.
    rc = display->change_resolution(display, display->width,
        display->height, display->stride);

    //If we couldn't set the resolution, fail out!
    if(unlikely(rc)) {
        openxtfb_error("Failed to activate a post-reconnect display!\n");
        return -EAGAIN;
    }

    //Finally, invalidate the display's existing state, by:
    //- Invalidating the entire screen, forcing a redraw; and
    //- Sending a cursor visibility update, which will force a
    //  reupload of the cursor image.
    display->invalidate_region(display, 0, 0, display->width, display->height);
    display->set_cursor_visibility(display, display->cursor.visible);

    //Since we've just reconnected, it's very likely that the appropriate 
    //geometry has changed. Send a hotplug event so the userland can check
    //to see if it needs to reshape itself.
    openxtfb_send_hotplug_event();

    //Indicate success.
    return 0;
}



/**
 * Determines whether the hardware can currently support creation of an OpenXT
 * framebuffer.
 *
 * @param pdev The platform device whose status should be queries.
 * @return 0 if we can support an OpenXT framebuffer, or an error code otherwise.
 */
static int openxtfb_probe(struct platform_device *pdev)
{
    //For now, we'll always support creation, as we only allow module insertion
    //if a display handler connection is available. If the connection has since
    //dropped, we'll pick up our framebuffer on the next reconnect. This
    //currently seems like the best way to provide a consistent experience
    //to the end user.
    __PV_HELPER_TRACE__;
    return 0;
}



/**
 * Handles removal of an OpenXT platform device, allowing the device to perform
 * cleanup of its resources prior to its final removal.
 *
 * Note that we allow the empty  platform device to remain until the module is
 * fully removed-- this simplifies the overall cleanup scheme.
 *
 * @param platform_device The platform device to be removed.
 * @param int 0 on success, or an error code if a full cleanup could not be
 *    completed. If this method returns an error, it will block the module
 *    from unloading, preventing resources from being leaked.
 */
static int openxtfb_remove(struct platform_device * platform_device)
{
    struct openxtfb_instance *instance;
    struct display_list_element *element, *temp;
    int rc = 0;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(platform_device, -EINVAL);

    //Get the PV display provider associated with the given device.
    instance = platform_get_drvdata(platform_device);

    //If we couldn't get the given provider, raise an error.
    if(!instance || !instance->provider) {
        openxtfb_error("Couldn't get a OpenXT framebuffer to destroy."
            " May be leaking memory!\n");
        return -EINVAL;
    }

    //Clean up each of the displays (framebuffers) associated with the platform device.
    list_for_each_entry_safe(element, temp, &instance->displays, node) {
        rc = openxtfb_clean_up_display(element->display);

        if(rc)
          break;
    }

    //If any of the clean-up operations failed, abort-- displays currently
    //depend on us.
    if(rc) {
        openxtfb_error("Failed to clean up a display--"
            " not removing the OpenXT Framebuffer module.\n");
        return rc;
    }

    //Indicate success.
    return 0;
}


/**
 * Stores a "size hint", which is used in future display creation as a
 * suggestion for display resolution.
 *
 * Note that this is considered a noncritical function-- on failure, operation
 * should be able to continue with reduced functionality (guests will fall back
 * to the default resolution).
 *
 * @param instance The OpenXT platform instance for which the display hint
 *    should be set.
 * @param key The DH unique key identifying the target display.
 * @param width The suggested width for the display.
 * @param height The suggested height for the display.
 * @return int 0 on success, or an error code on failure.
 */
static int __set_display_hint_for_display(struct openxtfb_instance * instance,
    uint32_t key, uint32_t width, uint32_t height)
{
    struct openxtfb_display_size_hint * hint_entry;

    __PV_HELPER_TRACE__;
    openxtfb_checkp(instance, -EINVAL);
    mutex_lock(&instance->lock);

    //Iterate over each of the /existing/ hint entries.
    list_for_each_entry(hint_entry, &instance->display_hints, node) {

        //If we've found one that matches our existing hint, update it,
        //and return.
        if(hint_entry->key == key) {
          hint_entry->width = width;
          hint_entry->height = height;

          mutex_unlock(&instance->lock);
          return 0;
        }
    }

    //Otherwise, if we haven't found a hint entry, we'll need to create one.
    hint_entry = kzalloc(sizeof(*hint_entry), GFP_KERNEL);

    //If we couldn't create a hint entry, bail out.
    //This isn't a critical error-- we can continue with reduced functionality.
    if(!hint_entry) {
        openxtfb_error("Could not create a new display size hint."
            " Continuing with reduced functionality...\n");
        mutex_unlock(&instance->lock);
        return -ENOMEM;
    }

    //Set up the new hint entry...
    hint_entry->key    = key;
    hint_entry->width  = width;
    hint_entry->height = height;

    //... and insert it into our linked list.
    list_add(&hint_entry->node, &instance->display_hints);

    mutex_unlock(&instance->lock);
    return 0;
}


/**
 * Clears the list of size hints, freeing all relevant memory and resetting all
 * display widths/heights to default.
 *
 * @param openxtfb_instance The platform device instance whose hints should be
 *    cleared.
 */
static void __destroy_all_display_hints(struct openxtfb_instance * instance)
{
    struct openxtfb_display_size_hint *hint_entry, *next_hint_entry;

    __PV_HELPER_TRACE__;
    mutex_lock(&instance->lock);

    //Remove each list entry from the list, and free its object.
    list_for_each_entry_safe(hint_entry, next_hint_entry, &instance->display_hints, node) {
        list_del(&hint_entry->node);
        kfree(hint_entry);
    }

    mutex_unlock(&instance->lock);
}


/**
 * Sends a udev event indicating that one or more of our framebuffers
 * may have changed resolution. This event allows the userspace to
 * adjust its mode, if necessary.
 */
static void openxtfb_send_hotplug_event(void)
{
    //Create our event information...
    char *event_string = "HOTPLUG=1";
    char *envp[] = { event_string, NULL };

    //... and send the event. Note that our framebuffer devices aren't
    //full-fledged kernel objects, so we can't easily use them to originate
    //the events. Instead, events are sent via our platform device.
    kobject_uevent_env(&openxtfb_device->dev.kobj, KOBJ_CHANGE, envp);
}


/**
 * Handles a change in the host display list. This gives us an opportunity to
 * react to any new or changed displays.
 *
 * @param display The display provider for which a host display is changing.
 * @param displays An array of dh_display_info structures, which describe the
 *    host displays.
 * @param num_displays The number of display advertisements received from the
 *    host.
 */
static void openxtfb_host_displays_changed(struct pv_display_provider * provider,
    struct dh_display_info * displays, uint32_t num_displays)
{
    int i;
    struct openxtfb_instance * instance = platform_get_drvdata(openxtfb_device);

    __PV_HELPER_TRACE__;
    openxtfb_checkp(provider);
    openxtfb_checkp(displays);
    openxtfb_checkp(instance);

    if(!ignore_hints) {

      //Itereate over each provided display, and update the display's hint.
      for(i = 0; i < num_displays; ++i) {
          struct dh_display_info * display = &displays[i];

          __set_display_hint_for_display(instance, display->key,
              display->width, display->height);
      }

      //And a "hotplug" event to the userspace, which applications (e.g. X)
      //can use to determine if they should change resolutions.
      openxtfb_send_hotplug_event();

    }

    //XXX Placeholder XXX
    //For now, advertise willingness to accept all displays, as is.
    provider->advertise_displays(provider, displays, num_displays);
}

/**
 * Returns true iff we should reconnect the given display.
 *
 * @param existing_display The display to be considered for reconnection...
 * @param request The add_display request that should be considered.
 */
static bool __add_display_requires_reconnect(struct pv_display * existing_display, 
    struct dh_add_display * request, bool connected)
{
    //TODO: To be more robus, this would also check to see if any of the ports
    //have changed. Unfortunately, the display helper doesn't yet expose
    //these to us. That should be changed in the display helper, and then
    //this should be updated.
    return (request->key != existing_display->key) || !connected;
}


/**
 * Handles a Display Handler "Add Display" request, which effectively asks us to
 * create a new framebuffer device. We can theoretically add as many
 * framebuffers as we'd like-- we don't arbitrary limit the framebuffer count.
 *
 * @param provider The display provider that should create the given display.
 * @param request The "Add Display" request to be processed.
 */
static void openxtfb_add_display_request(struct pv_display_provider * provider,
    struct dh_add_display * request)
{
    uint32_t width;
    uint32_t height;

    struct pv_display * existing_display;

    struct openxtfb_instance * instance = platform_get_drvdata(openxtfb_device);

    __PV_HELPER_TRACE__;
    openxtfb_checkp(provider);
    openxtfb_checkp(request);
    openxtfb_checkp(instance);

    //First, determine a good width and height for the given display.
    __get_recommended_size_for_display(instance, request->key, &width, &height);

    //Check to see if we already have a display associated with the request's
    //unique key.
    existing_display = __find_add_display_target(instance, request->key);

    //If we already have a display that can render the existing connection.
    if(existing_display) {
        //If set-up for the relevant display would require a reconnect (e.g. because
        //we've lost connection, or the connection details have changed), reconnect
        //to the provided IVC ports.
        openxtfb_debug("Host wants to reconnect to display %u on port %u.\n", 
                       (unsigned int)request->key, (unsigned int)request->framebuffer_port);

        openxtfb_reconnect_display_to_backend(existing_display, request);
    }
    //Otherwise, if we don't have an exsiting display, this is a new request,
    //and we should create a brand new framebuffer.
    else {
        openxtfb_debug("Host wants to add %ux%u display (%u) on port %u.\n",
            (unsigned int)width, (unsigned int)height,
            (unsigned int)request->key, (unsigned int)request->framebuffer_port);

        openxtfb_create_framebuffer(provider, request, width, height);
    }
}



/**
 * Deferred connection callback.
 *
 * Attempts to initiate a deferred connection, potentially reconnecting
 * to the display handler after a failed or interrupted connection.
 *
 * @param data A pointer to the openxfb instance in question.
 */
static void openxtfb_deferred_connect(struct work_struct *work)
{

    int rc;
    struct openxtfb_instance *instance = platform_get_drvdata(openxtfb_device);
    (void)work;

    __PV_HELPER_TRACE__;

    mutex_lock(&instance->lock);

    //First, if we have an existing provider, tear it down.
    if(instance->provider) {
      instance->provider->destroy(instance->provider);
      instance->provider = NULL;
    }

    //Attempt to create our connection to the display handler.
    rc = create_pv_display_provider(&instance->provider,
        (domid_t)__get_display_domid(), (uint16_t)control_port);

    //If we were able to make a display connection, initialize it.
    if(!rc) {
        __openxtfb_finalize_connection(instance->provider);
    }

    mutex_unlock(&instance->lock);

    //If we weren't able to make a display connection,
    //wait for the retry delay, and try again.
    if(rc) {
       openxtfb_debug("Couldn't reconnect. Queuing another try ...\n");
       openxtfb_defer_connect(reconnect_delay);
    }
}


/**
 * Schedules a deferred (re)connect; attempting to forge a display handler
 * connection after the provided delay.
 *
 * @param instance The openxtfb instance for which the reconnect should
 *    be attempted.
 * @param delay The delay, in jiffies, until the reconnect is attempted.
 *    This delay will be used for the first reconnect attempt. If an
 *    attempt is not made, reconnection will continue until the
 *    connection is made, with a delay of reconnect_delay between
 *    subsequent attempts.
 *
 */
static void openxtfb_defer_connect(int delay)
{
    struct openxtfb_instance *instance = platform_get_drvdata(openxtfb_device);
    __PV_HELPER_TRACE__;

    mutex_lock(&instance->lock);
    schedule_delayed_work(&instance->reconnect_work, delay);
    mutex_unlock(&instance->lock);
}


/**
 * Cancels any existing deferred connection attempts.
 * Should be called upon a successful connection, or before any
 * new deferred connections are added to the queue.
 *
 * Assumes the instance lock is held.
 */
static void __openxtfb_cancel_deferred_connect(void)
{
    struct openxtfb_instance *instance = platform_get_drvdata(openxtfb_device);
    __PV_HELPER_TRACE__;

    cancel_delayed_work(&instance->reconnect_work);
}


/**
 * Cancels any existing deferred connection attempts.
 * Should be called upon a successful connection, or before any
 * new deferred connections are added to the queue.
 */
static void openxtfb_cancel_deferred_connect(void)
{
    struct openxtfb_instance *instance = platform_get_drvdata(openxtfb_device);
    __PV_HELPER_TRACE__;

    mutex_lock(&instance->lock);
    __openxtfb_cancel_deferred_connect();
    mutex_unlock(&instance->lock);
}


/**
 * Handles any error relevant to our PV display provider serious enough to
 * require renegotiation of our connection.
 *
 * @param provider The provider that suffered a fatal error.
 */
static void openxtfb_handle_provider_error(struct pv_display_provider * provider)
{
    struct display_list_element *entry;
    struct openxtfb_instance *instance = platform_get_drvdata(openxtfb_device);

    __PV_HELPER_TRACE__;

    openxtfb_error("Control connection error! Attempting to reconnect...\n");

    //Once the control channel goes down, it's likely that the connections
    //to each of the guest display aren't stable. In this case, we'll mark
    //each of the display connections as disconnected-- which means that the
    //Display Handler will attempt to initiate a reconnect when the control
    //channel comes back.
    
    //Iterate over each of the /existing/ hint entries...
    mutex_lock(&instance->lock);
    list_for_each_entry(entry, &instance->displays, node) {
        entry->connected = false;
    }
    mutex_unlock(&instance->lock);

    //Schedule a deferred reconnect.
    openxtfb_cancel_deferred_connect();
    openxtfb_defer_connect(reconnect_delay);
}


/**
 * Handles any error relevant to a PV display serious enough to require
 * require renegotiation of our connection.
 *
 * @param provider The display that suffered a fatal error.
 */
static void openxtfb_handle_display_error(struct pv_display * display, void* userData)
{
    __PV_HELPER_TRACE__;

    //Mark the connection as disconnected.
    __update_display_connection_status(display, false);

    //Note: we no longer try to re-establish the whole display connection
    //when a display goes down, as the Display Handler now tears down connections
    //in several non-error cases, including switching between extended and cloned
    //modes.
    //
    //If we wind up having issues with reconnect, we may want to re-send the
    //advertise capabilities packet over the _existing_ provider channel,
    //re-starting the display negotiation process.
}


/**
 * Handles a system shutdown notification. For now, we'll ignore shutdown events,
 * and handle connection closure in the device removal code.
 *
 * @param dev The platform device affected by the shutdown notification.
 */
static void openxtfb_shutdown(struct platform_device *dev)
{
    __PV_HELPER_TRACE__;
}


/**
 * Handles placing the OpenXT framebuffer driver into a suspended state.
 * breaking all PV connections until the domain is resumed.
 *
 * @param dev The OpenXTFB platform device being placed into suspend.
 */
static int openxtfb_suspend(struct platform_device *dev, pm_message_t msg)
{
    //For now, we don't explicitly support VM suspend, as the exact behavior
    //of IVC during suspend is still up in the air. If IVC can correctly close
    //all connections on suspend, the reconnet handler should automatically
    //handle suspend and resume.
    __PV_HELPER_TRACE__;
    return 0;
}


/**
 * Handles waking the OpenXT framebuffer driver from a suspended state.
 * This should renegotatite all IVC connections with the display handler.
 *
 * @param dev The OpenXT framebuffer platform device being awoken.
 */
static int openxtfb_resume(struct platform_device *dev)
{
    //TODO: Trigger the reconnect handler here, if it hasn't already been
    //triggered. For now, we don't support reconnect, so we just awkwardly hang
    //there after a resume.
    __PV_HELPER_TRACE__;
    return 0;
}

/**
 * The OpenXT framebuffer's platform device functions.
 */
static struct platform_driver openxtfb_driver = {
    .probe    = openxtfb_probe,
    .remove   = openxtfb_remove,
    .shutdown = openxtfb_shutdown,
    .suspend  = openxtfb_suspend,
    .resume   = openxtfb_resume,
    .driver   = {
        .name = "openxtfb",
        .owner = THIS_MODULE,
    },
};

/******************************************************************************/
/* Init Functions                                                             */
/******************************************************************************/


/**
 * Validates the module's parameters.
 *
 * @returns 0 if all parameters are valid, or an error code otherwise.
 */
static int __validate_module_parameters(void)
{
    __PV_HELPER_TRACE__;

    if((control_port > 65535) || !control_port) {
        openxtfb_error("Control port must be between 1-65535, inclusive.\n");
        return -EINVAL;
    }

    if(__get_display_domid() > 65535) {
        openxtfb_error("Display domain must be a valid domid (0-65535, inclusive).\n");
        return -EINVAL;
    }

    if(advertised_framebuffers < 0) {
        openxtfb_error("The advertised framebuffer count must be non-negative.\n");
        return -EINVAL;
    }

    if((virtual_width <= 0) || (virtual_height <= 0)) {
        openxtfb_error("An impossible virtual size was specified!\n");
        return -EINVAL;
    }

    if(reconnect_delay <= 0) {
        openxtfb_error("Reconnect delay must be positive!\n");
        return -EINVAL;
    }

    //If neither had an issue, indicate success.
    return 0;
}

/**
 * Initialies a provider device, and begins the handshaking exchange with
 * the display handler.
 *
 * @param provider The provider to initialize.
 * @return 0 on success, or an error code on failure.
 */
static int __openxtfb_finalize_connection(struct pv_display_provider * provider)
{
    __PV_HELPER_TRACE__;

    //Register a handler for the events (and requests) sent by the Display Handler...
    provider->register_host_display_change_handler(provider,
        openxtfb_host_displays_changed);
    provider->register_add_display_request_handler(provider,
        openxtfb_add_display_request);

    //... and register a handler for fatal errors.
    provider->register_fatal_error_handler(provider,
        openxtfb_handle_provider_error);

    //Now that we're set up, advertise our capabilities to the Display Handler,
    //effectively beginning our handshake.
    return provider->advertise_capabilities(provider, advertised_framebuffers);
}


/**
 * Basic initialization code. This code is called when the driver is loaded
 * into the kernel. This basically tells the kernel what our "probe" function
 * is which will eventually fire up the framebuffer code.
 */
static int __init openxtfb_init(void)
{
    int rc;

    struct openxtfb_instance * instance;
    struct pv_display_provider * provider;

    __PV_HELPER_TRACE__;

    if(__validate_module_parameters())
        return -EINVAL;

    //If no value was provided for ignore_keys, assume a sane default:
    //apply ignore_keys iff we can only provide a single display.
    if(ignore_keys == -1)
        ignore_keys = (advertised_framebuffers == 1);

    //First, attempt to create a state object for this module.
    instance = kzalloc(sizeof(*instance), GFP_KERNEL);

    //If we couldn't allocate an object, fail out.
    if(!instance) {
        openxtfb_error("Could not allocate enough memory to store the "
            "module's state. Not loading.\n");
        return -ENOMEM;
    }

    //Initialize the linked list that will store our display hints,
    //our list of displays, a the lock object that will protect our
    //internals.
    INIT_LIST_HEAD(&instance->displays);
    INIT_LIST_HEAD(&instance->display_hints);
    mutex_init(&instance->lock);

    //Initialize the work object we'll use for deferred reconnects.
    //This is mostly commonly used in the event of a disconnect, and
    //is used to recreate our main connection to the display handler--
    //the PV display provider.
    INIT_DELAYED_WORK(&instance->reconnect_work, openxtfb_deferred_connect);

    //Next, attempt to create our pv_display_provider object.
    rc = create_pv_display_provider(&instance->provider,
        (domid_t)__get_display_domid(), (uint16_t)control_port);

    //If we weren't able to make a PV connection, fail out.
    if(rc && !retry_initial_connect) {
        openxtfb_error("Could not connect to the remote display domain! "
            "Module will not be loaded.\n");
        return -ENXIO;
    } 
    else if(rc) {
        openxtfb_error("Could not connect to the display domain! "
            "Connection will be deferred.\n");
        instance->provider = NULL;
    }

    //Get a shortcut to the given display provider.
    provider = instance->provider;

    //Register our platform _driver_, which provides the infrasturcture for
    //dealing with our paravirtualized devices.
    rc = platform_driver_register(&openxtfb_driver);

    //If we couldn't register our driver, fail out!
    if (rc) {
      openxtfb_error("Failed to register the OpenXT platform driver.\n");

      //Clean up.
      if(provider)
          provider->destroy(provider);
      return -ENOMEM;
    }

    //And create the actual PV display device. This display device is 
    //responsible for all PV framebuffers ("displays") created on this domain.
    openxtfb_device = platform_device_register_simple("openxtfb", 0, NULL, 0);

    //If we couldn't create a device, error out.
    if (IS_ERR(openxtfb_device)) {
        openxtfb_error("Failed to register an OpenXT Framebuffer "
            "platform device!\n");

        //Clean up.
        if(provider)
            provider->destroy(provider);

        platform_driver_unregister(&openxtfb_driver);
        return PTR_ERR(openxtfb_device);
    }

    //Bind our new instance to the platform device-- this will allow us to
    //access its data from the platform device's "methods".
    platform_set_drvdata(openxtfb_device, instance);

    //If we have a provider, finish setting up the connection.
    if(provider) {
        rc = __openxtfb_finalize_connection(provider);
    }
    //Otherwise, defer the initial connection; we'll create our
    //provider (and finalize it) when the display handler comes up.
    else {
        openxtfb_defer_connect(reconnect_delay);
        rc = 0;
    }

    //If we couldn't initiate the handshaking, but we previous had a connection,
    //defer the connection, and try again.
    if(rc) {
        openxtfb_error("An error occurred during handshaking. Deferring connection...\n");
        openxtfb_defer_connect(reconnect_delay);
    }

    //Finally, indicate success.
    return 0;
}


/**
 * Unloads our platform device from the kernel. Called on module removal.
 */
static void __exit openxtfb_exit(void)
{
    struct openxtfb_instance * instance = platform_get_drvdata(openxtfb_device);

    __PV_HELPER_TRACE__;

    openxtfb_cancel_deferred_connect();

    //Clean up all of our size hints.
    __destroy_all_display_hints(instance);

    //If we have a provider to clean up, clean it up!
    if(instance->provider)
      instance->provider->destroy(instance->provider);

    //Clean up our instance information...
    kfree(instance);

    //... and tear down our platform device.
    platform_device_unregister(openxtfb_device);
    platform_driver_unregister(&openxtfb_driver);
}

//Register our initialization and exit functions, exporting the symbols
//used to link our module to the kernel.
module_init(openxtfb_init);
module_exit(openxtfb_exit);

/******************************************************************************/
/* Kernel Macros                                                              */
/******************************************************************************/

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("OpenXT virtual framebuffer for Xen");
